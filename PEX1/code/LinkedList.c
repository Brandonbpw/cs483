/** LinkedList.c
 * ===========================================================
 * Name: Brandon Webster, Dr. Brown
 * Section: M3A
 * Project: Implement a list using pointers.
 * Purpose: General purpose list implementation.
 * ===========================================================
 */

#include "LinkedList.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/** ----------------------------------------------------------
 * Create a new linked list.
 * @return The address of a new linked list
 */
LinkedList * linkedListCreate() {
    LinkedList * my_list = NULL;

    my_list = (LinkedList *) malloc(sizeof(LinkedList));
    my_list->first = NULL;
    my_list->last = NULL;
    my_list->numberElements = 0;

    return my_list;
}

/** ----------------------------------------------------------
 * Delete a linked list.
 * @param list
 */
void linkedListDelete(LinkedList * list) {
    // delete all of the nodes in the list
    Node * nodePointer = list->first;
    Node * nextNode = NULL;

    while (nodePointer != NULL) {
        nextNode = nodePointer->next;
        free(nodePointer);
        nodePointer = nextNode;
    }

    // This is not necessary, because the list should not
    // be accessed any more, but this makes sure that if
    // it was, the list would be empty.
    list->first = NULL;
    list->last = NULL;
    list->numberElements = 0;

    // Delete the meta-data for the list.
    free(list);
}

/** ----------------------------------------------------------
 * Append an new element on the end of the list.
 * @param list - the linked list
 * @param element - the new element to be added to the list.
 */
void linkedListAppend(LinkedList * list, ElementType  element) {
    Node *newNode= (Node*)malloc(sizeof(Node));
    newNode->data = strdup(element);
    newNode->next = NULL;

    if(list->first == NULL){
        list->first=newNode;
    } else {
        list->last->next = newNode;
    }
    list->last = newNode;
    list->numberElements +=1;
}

/** ----------------------------------------------------------
 * Insert a new element into a linked list at the desired position
 * @param list - the linked list
 * @param position - the position in the list where the new element is to be added.
 * @param element - the new element for the list
 */
void linkedListInsertElement(LinkedList * list, int position, ElementType element) {
    Node *newNode = (Node *) malloc(sizeof(Node));
    newNode->data = element;
    newNode->next = NULL;
    if (position < list->numberElements) {
        if (position != 0) {
            Node *position_shift = list->first;

            for (int i = 0; i < position-1; i++) {
                position_shift = position_shift->next;
            }
            newNode->next = position_shift->next;
            position_shift->next= newNode;

        } else {
            Node *position_shift = list->first;
            list->first = newNode;
            newNode->next = position_shift;

        }
        list->numberElements += 1;
    }else{
        if (position == list->numberElements){
            linkedListAppend(list, element);
        }else{
            printf("error, position is too large");
        }
    }
}

/** ----------------------------------------------------------
 * Retrieve the element at the desired position.
 * @param list - the linked list
 * @param position - which position in the list to retrieve
 * @return a single element from the list from the desired position
 */
ElementType linkedListGetElement(LinkedList * list, int position) {
    Node* getNode= list->first;
    if(position ==0){
        return getNode->data;
    }else{
        for (int i = 0; i < position; i++) {
            getNode = getNode->next;
        }
    }
    return getNode->data;
}


/**----------------------------------------------------------
 * Delete specific element
 * @param list - the linked list
 * @param position - element to be deleted (closest to head)
 */
void linkedListDeleteElement (LinkedList *list, ElementType element){
    int position;
    position = linkedListFindElement(list, element);
    if(position != -1){
        linkedListDeleteElementn(list, position);
    }
    else{
        printf("Error"); //******* may need to change
    }
}

/** ----------------------------------------------------------
 * Delete an element from a linked list at a specific position
 * @param list - the linked list
 * @param position - the position in the array of the element to delete.
 */
void linkedListDeleteElementn(LinkedList * list, int position) {
    Node *current = list->first;
    if (list->numberElements == 1) {
        free(current);
        list->first = NULL;
        list->last = NULL;
        list->numberElements = 0;
    } else {
        if (position == 0) {
            free(list->first->data);
            list->first = list->first->next;
            list->numberElements-=1;
            free(current);

        }else if (position == list->numberElements-1){
            while(current->next->next != NULL)  {
                current = current->next;
            }
            free(current->next->data);
            free(current->next);
            current->next = NULL;
            list->numberElements-=1;
            list->last = current;

        }else if (position >= list->numberElements) {
            printf("Position outside range of elements");

        } else {

            for (int i = 0; i < position-1; i++) {
                current = current->next;
            }
            Node *nodeSkip = current->next->next;
            Node *temp_node = current->next;
            current->next = nodeSkip;
            free(temp_node->data);
            free(temp_node);

        }
    }
}


/** ----------------------------------------------------------
 * Print every element in the linked list.
 * @param list - the linked list
 */
void linkedListPrint(LinkedList *list) {
    Node* printNode = list->first;
    int i=0;
    while(printNode) {
        printf("%d. %s \n", i, printNode->data);
        i++;
        printNode = printNode->next;
    }
    printf("\n");
}
/**-----------------------------------------------------------
 * Prints data in nth node
 * @param list - the linked list
 * @param position - node position data being printed
 */
void linkedListPrintn (LinkedList *list, int position){
    Node* getNode= list->first;
    if(position ==0){
        printf("%s ", getNode->data);
    }else{
        for (int i = 0; i < position; i++) {
            getNode = getNode->next;
        }
    }
    printf("%s ", getNode->data);
}

/** ----------------------------------------------------------
 *
 * @param list - a pointer to a linked list
 * @param position - position in linked list to change value
 * @param newValue - value to be update at position
 */
void linkedListChangeElement(LinkedList *list, int position, ElementType newValue) {
    Node *changeNode= list->first;
    for (int i = 0; i < position ; i++) {
        changeNode = changeNode->next;
    }
    changeNode->data = newValue;
}
/** ----------------------------------------------------------
 * returns index of value in linked list
 * @param list - a pointer to a linked list
 * @param value - value being searched for
 * @return - index of value in linked list
 */
int linkedListFindElement(LinkedList *list, ElementType value) {
    Node *findElement= list->first;
    int index = 0;
    while(findElement != NULL){
        if(strcmp(findElement->data, value) == 0){
            return index;
        }else{
            index+=1;
        }
        findElement=findElement->next;
    }
        return -1;
}
