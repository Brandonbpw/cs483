#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "LinkedList.h"
#include <unistd.h>
#include <sys/wait.h>
#include <pwd.h>

/**
 * Doc Statement:
 * We used the following website to implement the cd shorthand command. The website talked about how tilde
 * expansion is handled by the shell and not a system call. The website then recommended to pass chdir getenv("HOME")
 * to provide the path for chdir. After reading this suggestion, we implemented chdir(getenv("HOME")) within our program.
 * https://www.linuxquestions.org/questions/programming-9/chdir-~-to-%24home-in-c-programming-language-4175457202/
 *
 * We also used the following website to print the name of the user upon startup. The source recommended utilizing getenv and
 * "USERNAME" to access the current user of the program.
 * After looking at this source, we implemented a printf statement that calls getenv("USER").
 *https://stackoverflow.com/questions/24503466/how-do-i-get-the-user-name-of-the-current-user
 *
 * We received help from Maj. Brault on strtok by helping us fix our malloc issues
 *
 * Chris Darcy helped explain how to use char** datatypes and how that relates to strtok
 */

void executeCommand(char ** array, LinkedList *history);
void change_dir(char ** array);
void other_commands(char ** array);
void recall(char**tmp_ptr, LinkedList *history, char * data);
//global exit condition
int exit_condition = 0;

int main(int argc, char **argv) {
    // creates linked list for history and recall
    LinkedList *history = linkedListCreate();

    // prints currrent directory
    char directory[512];
    printf(getcwd(directory, sizeof(directory)));
    printf("\n %s", getenv("USER"));

//    printf("%s",getlogin());

    char *token;

    /* get the first token */


    while (exit_condition != 1) {
        char input_str[64];

        char *data = malloc(64 * sizeof(char *));
        char **tmp_ptr = malloc(sizeof(char *));

        // gets user input
        printf("\n enter a command: ");
        fgets(input_str, sizeof(input_str), stdin);
        input_str[strlen(input_str)-1]=0;
        strcpy(data, input_str);

        int i = 0;

        // puts user input into an array.
        tmp_ptr[i] = malloc(sizeof(char *));
        strcpy(tmp_ptr[i],strtok(input_str, " "));
        while (tmp_ptr[i++] != NULL) {
            tmp_ptr[i] = malloc(sizeof(char *));
            char *temp = strtok(NULL, " ");
            tmp_ptr[i] = temp; //fix this
        }
        if (linkedListFindElement(history, data) == -1) {
            linkedListAppend(history, data);
        } else {
            linkedListDeleteElement(history, data);
            linkedListAppend(history, data);
        }
        int n = 0;

        // adds command input into linked list
        //if recall
        //use string to find linked list history data
        //pass data to recall ptr to be executed in loop
        if (strcmp(tmp_ptr[0], "recall") == 0) {
            recall(tmp_ptr, history, data);
        }else{
            executeCommand(tmp_ptr, history);
        }
        free(tmp_ptr);
    }
    linkedListDelete(history);
}

void executeCommand(char ** array, LinkedList *history) {
    if (strcmp("exit", array[0]) == 0) {
        printf("exiting..... \n");
        exit_condition = 1;
    } else if (strcmp("history", array[0]) == 0) {
        printf("getting history.....\n");
        linkedListPrint(history);
    } else if (strncmp("cd", array[0], 2) == 0) {
        if(strcmp("~", array[1]) == 0) {
            if(chdir(getenv("HOME")) == 0){
                char directory[512];
                printf(getcwd(directory, sizeof(directory)));
            }
        }
        printf("changing directory.....\n");
        change_dir(array);
    } else {
        other_commands(array);
  }
}

void recall(char**tmp_ptr, LinkedList *history, char* data) {


    int position = atoi(tmp_ptr[1]);
    if (history->numberElements > position) {
        char *recall_data = malloc(64 * sizeof(char *));
        char *recall_string = malloc(64 * sizeof(char *));
        char **recall_ptr = malloc(sizeof(char *));
        int i = 0;

        strcpy(recall_data, linkedListGetElement(history, position));
        strcpy(recall_string, recall_data);

        recall_ptr[i] = malloc(sizeof(char *));
        recall_ptr[i++] = strtok(recall_data, " ");
        while (tmp_ptr[i - 1] != NULL) {
            recall_ptr[i] = malloc(sizeof(char *));
            recall_ptr[i++] = strtok(NULL, " ");
        }
        if (linkedListFindElement(history, recall_string) == -1) {
            linkedListAppend(history, recall_string);
        } else {
            linkedListDeleteElement(history, recall_string);
            linkedListAppend(history, recall_string);
        }

        if (strcmp(recall_ptr[0], "recall") == 0) {
            printf("Cannot recall a recall");
        }else{
            executeCommand(recall_ptr, history);
        }
        free(recall_ptr);
    }
    else{
        printf("No command found in nth position");
    }
}



void change_dir(char ** array){
    printf("%s", *array);
        if(chdir(array[1]) == 0){
            char directory[512];
            printf(getcwd(directory, sizeof(directory)));
        }else{
            printf("Error, no such path exists");
        }
    }


void other_commands(char ** array ) {
    pid_t ret_val;
    ret_val = fork();
    if (ret_val == -1) {
        printf("failed");
    }
    if (ret_val == 0) {
        execvp(array[0], array);
    } else {
        wait(NULL);
    }

}

