/** LinkedList.h
 * ===========================================================
 * Name: Brandon Webster, Dr. Brown
 * Section: M3A
 * Project: Implement a list using pointers.
 * Purpose: General purpose list implementation.
 * ===========================================================
 */

#ifndef LINKED_LIST_H
#define LINKED_LIST_H

// Define the data type for the elements that will be stored in this linked list.
// This definition simply makes it easier to change the type of data the list stores.
typedef char* ElementType;

// Define on node of the linked list
typedef struct node {
    ElementType   data;
    struct node * next;
} Node;

// Define the meta-data that stores the linked list.
typedef struct linkedList {
    Node * first;
    Node * last;
    int    numberElements;
} LinkedList;

// Functions that manipulate a linked list
LinkedList * linkedListCreate(); //Done
void         linkedListAppend         (LinkedList *list, ElementType element); //Done
void         linkedListInsertElement  (LinkedList *list, int position, ElementType element); //Done
void         linkedListDeleteElement  (LinkedList *list, ElementType element); //Done
void         linkedListDeleteElementn (LinkedList *list, int position); //Done
void         linkedListPrint          (LinkedList *list); //Done
void         linkedListPrintn         (LinkedList *list, int position); //Done
ElementType  linkedListGetElement     (LinkedList *list, int position);  //Done
void         linkedListDelete         (LinkedList * list); //Done


void         linkedListChangeElement  (LinkedList *list, int position, ElementType newValue); //Not used for now
int          linkedListFindElement    (LinkedList *list, ElementType value); //Not used directly

#endif // LINKED_LIST_H
